# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.2] - 2024-10-21
### Changed

* Update ameba
* Update crystal to 1.14.0

## [0.3.1] - 2018-02-04
### Changed

* Fix terminal output - [#7]

## [0.3.0] - 2018-02-04
### Added

* Implement logging to logfile - [#4]

## [0.2.0] - 2018-12-23
### Added

* `version` command to print the version number - [#1]

## [0.1.0] - 2018-12-15
### Added
Initial version with minimal feature set.

* Sets up ENV for restic according to the values in the config file
* `backup` command to create a backup
* `restic` command runs the restic executable with the specified command line arguments

[0.1.0]: https://gitlab.com/dilli/btrestic/tags/v0.1.0
[0.2.0]: https://gitlab.com/dilli/btrestic/tags/v0.2.0
[0.3.0]: https://gitlab.com/dilli/btrestic/tags/v0.3.0
[0.3.1]: https://gitlab.com/dilli/btrestic/tags/v0.3.1
[0.3.2]: https://gitlab.com/dilli/btrestic/tags/v0.3.2

[#1]: https://gitlab.com/dilli/btrestic/issues/1
[#4]: https://gitlab.com/dilli/btrestic/issues/4
[#7]: https://gitlab.com/dilli/btrestic/issues/7
