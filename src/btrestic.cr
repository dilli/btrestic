require "./btrestic/cli"

module Btrestic
  VERSION = {{system("git", "describe", "--tags", "--match", "v[0-9]*").stringify.gsub(/\Av/, "")}}
end

private def restic_subcommand_index
  ARGV.index { |arg| arg == "restic" }
end

private def args
  end_index = restic_subcommand_index
  return ARGV.dup if end_index.nil?
  ARGV[0..end_index]
end

private def restic_args
  start_index = restic_subcommand_index
  return [] of String if start_index.nil?

  ARGV[start_index + 1..-1]
end

Btrestic::Cli.new(args, restic_args).run
