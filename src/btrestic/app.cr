require "./settings"
require "log"

module Btrestic
  class App
    @settings : Btrestic::Settings
    @silent : Bool

    def initialize(config_file, silent = false)
      @settings = load_settings(config_file)
      @silent = silent
      setup_logger
    end

    def backup
      cleanup_snapshots_dir
      create_snapshots
      run_restic_backup
      cleanup_snapshots_dir
    end

    def restic(restic_args : Array(String))
      run_command("restic", restic_args, with_logging: false)
    end

    private def settings
      @settings
    end

    private def load_settings(config_file)
      settings_text = File.read(config_file)
      Btrestic::Settings.from_yaml(settings_text)
    end

    private def setup_logger
      backend = Log::IOBackend.new(File.new(settings.logging.logfile))
      Log.setup(Log::Severity.parse(settings.logging.loglevel), backend)
    end

    private def env
      ENV.to_h.dup.merge(settings.env)
    end

    private def cleanup_snapshots_dir
      snapshot_dir = settings.snapshot_dir
      Dir.children(snapshot_dir).each do |child|
        child_path = File.join(snapshot_dir, child)
        unless run_command("btrfs", ["sub", "del", child_path]).success?
          Log.error { "Error on deleting snapshot" }
          raise "Error on deleting snapshot"
        end
      end
    end

    private def create_snapshots
      snapshot_dir = settings.snapshot_dir
      settings.subvolumes.each do |subvolume|
        unless run_command("btrfs", ["sub", "snap", "-r", subvolume, snapshot_dir]).success?
          Log.error { "Error on deleting snapshot" }
          raise "Error on deleting snapshot"
        end
      end
    end

    private def run_restic_backup
      args = ["backup", snapshots_args, excludes_args, "-v"].flatten
      unless run_command("restic", args).success?
        Log.error { "Error on creating backup" }
        raise "Error on creating backup"
      end
    end

    private def snapshots_args
      Dir.children(settings.snapshot_dir).map { |snapshot| File.join(settings.snapshot_dir, "#{snapshot}") }
    end

    private def excludes_args
      settings.excludes.flat_map { |exclude| ["--exclude", exclude] }
    end

    private def run_command(name, args, with_logging = true)
      Log.info { %(Running "#{name} #{args.join(" ")}") }
      log_buffer = IO::Memory.new
      stdout_writer = with_logging ? IO::MultiWriter.new(STDOUT, log_buffer) : STDOUT
      stderr_writer = with_logging ? IO::MultiWriter.new(STDERR, log_buffer) : STDERR
      status = Process.run(name, args, output: stdout_writer, error: stderr_writer, env: env)
      if status.success?
        Log.info { log_buffer.to_s }
        Log.info { %(Completed "#{name} #{args.join(" ")}") }
      else
        Log.error { log_buffer.to_s }
        msg = %(Failed "#{name} #{args.join(" ")}")
        Log.error { msg }
        raise msg
      end
      status
    end
  end
end
