require "./app"
require "option_parser"

module Btrestic
  class Cli
    getter :args, :restic_args

    def initialize(@args : Array(String), @restic_args : Array(String)); end

    def run
      config_file = "/etc/btrestic.conf"
      backup_cmd = false
      restic_cmd = false
      version_cmd = false

      parser = OptionParser.new do |parser|
        parser.banner = "Usage: btrestic [subcommand] [arguments]"
        parser.on("backup", "Run backup") do
          backup_cmd = true
          parser.banner = "Usage: btrestic backup [arguments]"
        end
        parser.on("restic", "Run restic with configured env") do
          restic_cmd = true
          parser.banner = "Usage: btrestic restic [arguments]"
        end
        parser.on("version", "Print the version") do
          version_cmd = true
          parser.banner = "Usage: btrestic version"
        end
        parser.on("--config_file=FILE", "-c FILE", "Config file") { |file| config_file = file }
        parser.on("--help", "-h", "Show this help") do
          puts parser
          exit
        end
      end

      parser.parse(args)

      if backup_cmd
        Btrestic::App.new(config_file).backup
      elsif restic_cmd
        Btrestic::App.new(config_file).restic(restic_args)
      elsif version_cmd
        puts Btrestic::VERSION
      else
        puts parser
        exit(1)
      end
    end
  end
end
