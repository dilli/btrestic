require "yaml"

module Btrestic
  class Settings
    class Logging
      include YAML::Serializable

      property logfile : String = "/var/log/btrestic.log"
      property loglevel : String = "info"
    end
  end
end
