require "yaml"
require "./settings/logging"

module Btrestic
  class Settings
    include YAML::Serializable

    @[YAML::Field(key: ":snapshot_dir")]
    property snapshot_dir : String

    @[YAML::Field(key: ":subvolumes")]
    property subvolumes : Array(String)

    @[YAML::Field(key: ":excludes")]
    property excludes : Array(String)

    @[YAML::Field(key: ":env")]
    property env : Hash(String, String)

    @[YAML::Field(key: ":logging")]
    property logging : Btrestic::Settings::Logging
  end
end
