# btrestic

**btrestic** is a wrapper for using [restic][restic] on a btrfs filesystem.
For all configured subvolumes it creates read-only snapshots, backs them
up with restic, and removes the snapshots afterwards.

## Installation

TODO: Write installation instructions here

## Usage

`btrestic --help`

## Contributing

1. Fork it (<https://gitlab.com/dilli/btrestic/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Daniel Illi](https://gitlab.com/dilli) - creator and maintainer

[restic]: https://restic.net/