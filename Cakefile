require "file_utils"
require "yaml"

desc "Build btrestic binary"
task :build do
  Dir.mkdir("bin") unless Dir.exists?("bin")
  execute(
    cmd: "shards install",
    announce: "Installing shards...",
    success: "Shards installed!",
    error: "Shard install failed."
  )
  execute(
    cmd: "crystal build src/btrestic.cr -o bin/btrestic --release --static",
    announce: "Building binary...",
    success: "Binary built!",
    error: "Build failed."
  )
end

desc "Build deb/rpm packages"
task :package do
  unless File.exists?("bin/btrestic")
    log("Compiled binary bin/btrestic does not exist! (hint: run build task first)", 1)
    exit
  end

  version = btrestic_version

  build_package("deb", version)
  build_package("rpm", version)
end

desc "Upload the deb/rpm packages to bintray"
task :publish do
  invoke! :clean
  invoke! :build
  invoke! :package
  bintray_publish
end

desc "Clean up all generated files (bin/*, packages/*)"
task :clean do
  FileUtils.rm_r("bin") if Dir.exists?("bin")
  FileUtils.rm_r("packages") if Dir.exists?("packages")
end

def build_package(type, version)
  Dir.mkdir("packages") unless Dir.exists?("packages")
  FileUtils.cd("packages") do
    execute(
      cmd: "fpm -f -s dir -t #{type} -n btrestic -v #{version} ../bin/btrestic=/usr/bin/btrestic",
      announce: "Building #{type} package...",
      success: "#{type} package built!",
      error: "#{type} package failed."
    )
  end
end

def btrestic_version
  output = IO::Memory.new
  Process.run("bin/btrestic", ["version"], output: output)
  output.close
  output.to_s.chomp
end

def bintray_config
  File.open("bintray.conf") do |file|
    YAML.parse(file)
  end
end

def bintray_publish
    bintray_cfg = bintray_config
    user = bintray_cfg["user"]
    apikey = bintray_cfg["apikey"]
    version = btrestic_version

    Dir.cd("packages") do
      deb_file = Dir["*.deb"].first
      bintray_publish_deb(deb_file, version, user, apikey)
      rpm_file = Dir["*.rpm"].first
      bintray_publish_rpm(rpm_file, version, user, apikey)
    end
end

def bintray_publish_rpm(file, version, user, apikey)
  execute(
    cmd: "curl -T #{file} -u#{user}:#{apikey} https://api.bintray.com/content/btrestic/rpm/btrestic/v#{version}/#{file};publish=1;override=1",
    announce: "Publishing rpm package...",
    success: "Rpm package published!",
    error: "Publishing rpm package failed."
  )
end

def bintray_publish_deb(file, version, user, apikey)
  execute(
    cmd: "curl -T #{file} -u#{user}:#{apikey} https://api.bintray.com/content/btrestic/deb/btrestic/v#{version}/#{file};deb_distribution=ubuntu;deb_component=main;deb_architecture=i386,amd64;publish=1;override=1",
    announce: "Publishing deb package...",
    success: "Deb package published!",
    error: "Publishing deb package failed."
  )
end